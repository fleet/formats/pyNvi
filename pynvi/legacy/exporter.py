import abc
import logging
from abc import ABC
from datetime import datetime
from enum import Enum
from os import path, PathLike
from typing import List, Tuple

import numpy as np
from osgeo import gdal, ogr, osr

from pynvi.legacy import nvi_driver

# Constant fields
FILE_FIELD = "FicData"
PROFILE_FIELD = "Profil"
START_DATE_FIELD = "DateDeb"
END_DATE_FIELD = "DateFin"
START_TIME_FIELD = "HeureDeb"
END_TIME_FIELD = "HeureFin"
START_LAT_FIELD = "LatDeb"
START_LON_FIELD = "LonDeb"
END_LAT_FIELD = "LatFin"
END_LON_FIELD = "LonFin"
CAMPAIGN_NAME = "Campagne"
CAMPAIGN_NUM = "NumCamp"
NAVIGATION = "Navigation"
TOOL = "Outil"

gdal.UseExceptions()


# pylint:disable=broad-exception-raised


class ShapeExportType(Enum):
    POINT = 0
    POLYLINE = 1


class Convert2Shp(ABC):
    """
    This class provides methods to convert netcdf navigation files to shape files.
    """

    def __init__(
        self,
        input_files: List[PathLike],
        output_file: PathLike,
        overwrite: bool = False,
        campaign: str = None,
        campaign_number: str = None,
        navigation: str = None,
        tool: str = None,
        export_type: ShapeExportType = ShapeExportType.POLYLINE,
        logger=None,
    ):
        """
        Initialize parameters.
        """
        self.input_files = input_files
        self.output_file = output_file
        self.added_const_fields = {}
        if campaign is not None:
            self.added_const_fields[CAMPAIGN_NAME] = campaign
        if campaign_number is not None:
            self.added_const_fields[CAMPAIGN_NUM] = campaign_number
        if navigation is not None:
            self.added_const_fields[NAVIGATION] = navigation
        if tool is None:
            self.added_const_fields[TOOL] = tool
        self.export_type = export_type

        self.overwrite = overwrite
        if logger is None:
            self.logger = logging.getLogger(self.__class__.__name__)
        else:
            self.logger = logger

    @abc.abstractmethod
    def get_navigation_data_values(
        self, filename
    ) -> Tuple[np.ndarray, np.ndarray, np.ndarray, datetime, datetime]:
        """This function retrieves valid navigation data from the given filename
        Return :
            longitudes, latitudes values as np.ndarray ,
            validity flag as a np.ndarray of boolean
            start,stop dates as datetime
        """

    # pylint: disable=too-many-nested-blocks
    def __call__(self):
        """
        Main method : converts NVI/MBG to a shape file.
        """

        # shape file driver
        shp_driver = ogr.GetDriverByName("ESRI Shapefile")
        layer_mapper = {
            ShapeExportType.POINT: ogr.wkbMultiPoint,
            ShapeExportType.POLYLINE: ogr.wkbLineString,
        }
        # data source
        if path.exists(self.output_file):
            if self.overwrite:
                shp_driver.DeleteDataSource(self.output_file)
            else:
                print("Error: file already exists: " + str(self.output_file))
                raise IOError("Error: file already exists: " + str(self.output_file))

        try:
            data_source = shp_driver.CreateDataSource(self.output_file)

            if data_source is None:
                print("Error while opening file " + str(self.output_file))
                raise IOError("Error while opening file " + str(self.output_file))

            # spatial reference, WGS84
            srs = osr.SpatialReference()
            srs.ImportFromEPSG(4326)

            # creation layer
            basename, extension = path.splitext(self.output_file)
            layer_name = path.split(basename)[-1]
            layer = data_source.CreateLayer("NAV", srs, layer_mapper[self.export_type])

            if layer is None:
                msg = f"Could not create layer {layer_name}"
                raise Exception(msg)

            # create fields
            layer.CreateField(ogr.FieldDefn(FILE_FIELD, ogr.OFTString))
            layer.CreateField(ogr.FieldDefn(PROFILE_FIELD, ogr.OFTString))
            layer.CreateField(ogr.FieldDefn(START_DATE_FIELD, ogr.OFTString))
            layer.CreateField(ogr.FieldDefn(END_DATE_FIELD, ogr.OFTString))
            layer.CreateField(ogr.FieldDefn(START_TIME_FIELD, ogr.OFTString))
            layer.CreateField(ogr.FieldDefn(END_TIME_FIELD, ogr.OFTString))
            layer.CreateField(ogr.FieldDefn(START_LON_FIELD, ogr.OFTReal))
            layer.CreateField(ogr.FieldDefn(START_LAT_FIELD, ogr.OFTReal))
            layer.CreateField(ogr.FieldDefn(END_LON_FIELD, ogr.OFTReal))
            layer.CreateField(ogr.FieldDefn(END_LAT_FIELD, ogr.OFTReal))
            layer.CreateField(ogr.FieldDefn(CAMPAIGN_NAME, ogr.OFTString))
            layer.CreateField(ogr.FieldDefn(CAMPAIGN_NUM, ogr.OFTString))
            layer.CreateField(ogr.FieldDefn(NAVIGATION, ogr.OFTString))
            layer.CreateField(ogr.FieldDefn(TOOL, ogr.OFTString))

            # log constant fields
            print("Exporting to shape file : ", self.output_file)
            for field_name, field_value in self.added_const_fields.items():
                print(field_name, ":", str(field_value))

            for filein in self.input_files:
                feature = ogr.Feature(layer.GetLayerDefn())

                # fill polyline and fields (abstract method specific for each input file type)
                (
                    lon,
                    lat,
                    validity,
                    start_date,
                    end_date,
                ) = self.get_navigation_data_values(filename=filein)

                # Populate polyline
                # Populate polyline
                if len(lat) > 1:
                    feature.SetField(FILE_FIELD, path.basename(filein))
                    feature.SetField(
                        PROFILE_FIELD, path.splitext(path.basename(filein))[0]
                    )
                    feature.SetField(START_DATE_FIELD, start_date.strftime("%d/%m/%Y"))
                    feature.SetField(END_DATE_FIELD, end_date.strftime("%d/%m/%Y"))
                    feature.SetField(START_TIME_FIELD, start_date.strftime("%H:%M:%S"))
                    feature.SetField(END_TIME_FIELD, end_date.strftime("%H:%M:%S"))
                    if self.export_type == ShapeExportType.POLYLINE:
                        geometry = ogr.Geometry(ogr.wkbLineString)
                        for lg, lt, flag in zip(lon, lat, validity):
                            # keep only valid points
                            valid_values = (
                                not np.isnan(lg)
                                and not np.isnan(lt)
                                and not (lg is np.ma.masked)
                                and not (lt is np.ma.masked)
                            )  # flag value shoud be enough but in some cases data is invalid even if point are marked as valid
                            if flag and valid_values:
                                geometry.AddPoint(lg, lt)
                        # set fields value
                        count = geometry.GetPointCount()
                        feature.SetField(START_LON_FIELD, geometry.GetPoint(0)[0])
                        feature.SetField(START_LAT_FIELD, geometry.GetPoint(0)[1])
                        feature.SetField(END_LON_FIELD, geometry.GetPoint(count - 1)[0])
                        feature.SetField(END_LAT_FIELD, geometry.GetPoint(count - 1)[1])
                        self.logger.info(
                            f"Create {filein} geometry : {geometry.GetPointCount()} points"
                        )
                        feature.SetGeometry(geometry)

                    else:
                        count = 0
                        geometry = ogr.Geometry(ogr.wkbMultiPoint)

                        for lg, lt, flag in zip(lon, lat, validity):
                            if flag:
                                point = ogr.Geometry(ogr.wkbPoint)
                                point.AddPoint(lg, lt)
                                if count == 0:
                                    feature.SetField(
                                        START_LON_FIELD, point.GetPoint(0)[0]
                                    )
                                    feature.SetField(
                                        START_LAT_FIELD, point.GetPoint(0)[1]
                                    )
                                count += 1
                                feature.SetField(END_LON_FIELD, point.GetPoint(0)[0])
                                feature.SetField(END_LAT_FIELD, point.GetPoint(0)[1])
                                geometry.AddGeometry(point)
                        self.logger.info(f"Create {filein} geometry : {count} points")
                        feature.SetGeometry(geometry)
                else:
                    self.logger.warning(f"Skip file {filein} having {len(lat)} points")

                # sets constant fields
                for field_name, field_value in self.added_const_fields.items():
                    feature.SetField(field_name, str(field_value))
                # Create the feature in the layer (shape file)
                layer.CreateFeature(feature)
        finally:
            data_source = None  # should close the files


class ConvertNvi2Shp(Convert2Shp):
    def get_navigation_data_values(
        self, filename
    ) -> Tuple[np.ndarray, np.ndarray, np.ndarray, datetime, datetime]:
        """
        Reads an NVI file, and fill the provided polyline and feature.
        """
        print("Input file (nvi) :", filename)
        with nvi_driver.nc_open(filename) as nvi:
            lon = nvi.get_longitudes()
            lat = nvi.get_latitudes()
            validity_flag = nvi.get_validities()

            start_date = nvi.get_start_date()
            end_date = nvi.get_end_date()

            return (lon, lat, validity_flag, start_date, end_date)
