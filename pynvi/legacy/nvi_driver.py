import warnings
from contextlib import contextmanager
from datetime import datetime
from enum import Enum, IntEnum
from os import PathLike
from typing import Generator, Optional

import netCDF4 as nc
import numpy as np

from pynvi.abstract_nvi_driver import AbstractNviDriver
from pynvi.version_2.nvi_driver import SensorQualityIndicator
from pynvi.version_2.quality_flag import QualityFlag

DATE = "mbDate"
TIME = "mbTime"

LATITUDES = "mbOrdinate"
LONGITUDES = "mbAbscissa"
ALTITUDES = "mbAltitude"
IMMERSION = "mbImmersion"
SPEED = "mbSpeed"
HEADING = "mbHeading"
TYPES = "mbPType"
QUALITY = "mbPQuality"
FLAG = "mbPFlag"


class NviSensorType(int, Enum):
    NOT_DEFINED = (0, "Not defined")
    GPS = (1, "GPS")
    GPS_DIFF = (2, "Differential GPS")
    TRANSIT = (3, "Transit")
    MINIRANGER = (4, "Miniranger")
    SEAFIX = (5, "Seafix")
    CINEMATIC_GPS = (6, "Cinematic GPS RTK")
    TRIDENT = (7, "Trident")
    AXYLE = (8, "Axyle")
    OPTIC = (9, "Optic")
    LORAN = (10, "Loran")
    DECCA = (11, "Decca")
    SYLEDIS = (12, "Syledis")
    RTK_FLOAT = (13, "RTK Float")

    def __new__(cls, value, label):
        obj = int.__new__(cls, value)
        obj._value_ = value
        obj.label = label
        return obj


class NviQualityFlag(IntEnum):
    """
    NVI legacy quality flags (store in mbPFlag variable).
    """

    REMOVED = -4
    DOUBTFUL_UNVALIDATED = -3
    VALID_UNVALIDATED = -2
    UNVALID = -1
    MISSING = 0
    DOUBTFUL = 1
    VALID = 2
    DOUBTFUL_VALIDATED = 3
    UNVALID_VALIDATED = 4
    REMOVED_VALIDATED = 5
    INTERPOLATED = 6
    MOVED = 7
    SMOOTHED = 8


class NviDriver(AbstractNviDriver):

    def __init__(self, file_path: str):
        self.file_path = file_path
        self.dataset = None

    @staticmethod
    def __get_utc_date(julian_date, julian_time) -> datetime:
        """
        Converts julian date to UTC
        """
        epoch = (julian_date - 2440588) * 24 * 3600 + (julian_time / 1000)
        return datetime.utcfromtimestamp(epoch)

    def nc_open(self, mode: str = "r") -> nc.Dataset:
        """
        Open the file and return the resulting Dataset
        """
        self.dataset = nc.Dataset(filename=self.file_path, mode=mode)
        return self.dataset

    def close(self):
        if self.dataset is not None:
            self.dataset.close()

    def get_file_path(self) -> PathLike | str | None:
        return self.file_path

    def get_times(self) -> np.ndarray:
        time_variable = self.dataset.variables[DATE]
        time_frac = self.dataset.variables[TIME]
        time_variable.set_auto_scale(
            False
        )  # disable autoscale to be able to set a starting date, force to 1970
        time_values = time_variable[:]
        time_frac_values = time_frac[:]

        npd = time_values.astype("timedelta64[D]")
        npd2 = time_frac_values.astype("timedelta64[ms]")
        time = np.datetime64("1970-01-01 00:00:00") + npd + npd2
        return time

    def get_end_date(self) -> datetime:
        """retrieve the date from metadata"""
        return self.__get_utc_date(
            self.dataset.getncattr("mbEndDate"), self.dataset.getncattr("mbEndTime")
        )

    def get_start_date(self) -> datetime:
        """retrieve the date from metadata"""
        return self.__get_utc_date(
            self.dataset.getncattr("mbStartDate"), self.dataset.getncattr("mbStartTime")
        )

    def get_latitudes(self) -> np.ndarray:
        return self.dataset.variables[LATITUDES][:]

    def get_longitudes(self) -> np.ndarray:
        return self.dataset.variables[LONGITUDES][:]

    def get_altitudes(self) -> np.ndarray:
        return self.dataset.variables[ALTITUDES][:]

    def get_immersion(self) -> np.ndarray:
        return self.dataset.variables[IMMERSION][:]

    def get_speeds(self) -> np.ndarray:
        speed_var = self.dataset.variables[SPEED]
        # Convert knots to m/s if necessary.
        return (
            speed_var[:] * 0.51444
            if "units" in speed_var.__dict__ and "knot" in speed_var.units
            else speed_var[:]
        )

    def get_headings(self) -> np.ndarray:
        return self.dataset.variables[HEADING][:]

    def get_courses_over_ground(self) -> Optional[np.ndarray]:
        return None

    def get_sensor_types(self) -> np.ndarray:
        """
        Return the sensor type values (see NviSensorType) :
            0 = NOT_DEFINED("Not defined")
            1 = GPS("GPS")
            2 =	GPS_DIFF("Differential GPS"),
            3 = TRANSIT("Transit")
            4 = MINIRANGER("Miniranger")
            5 =	SEAFIX("Seafix")
            6 = CINEMATIC_GPS("Cinematic GPS RTK")
            7 = TRIDENT("Trident")
            8 = AXYLE("Axyle")
            9 = OPTIC("Optic")
            10 = LORAN("Loran")
            11 = DECCA("Decca")
            12 = SYLEDIS("Syledis")
            13 = RTK_FLOAT("RTK Float")
        """
        warnings.filterwarnings(
            "ignore", message="Warning: missing_value not used since it"
        )
        types_var = self.dataset.variables["mbPType"]
        types_var.set_auto_scale(False)
        types_var.set_auto_chartostring(False)
        types = np.frombuffer(types_var[:], dtype=np.uint8)
        return types

    def get_sensor_quality_indicators(self) -> np.ndarray:
        """
        Mapping of NviSensorType (v1) to SensorQualityIndicator (v2)
        """
        nvi_sensor_types = self.get_sensor_types()

        result = np.full_like(
            nvi_sensor_types,
            fill_value=SensorQualityIndicator.FIX_NOT_AVAILABLE.numerator,
            dtype=int,
        )

        enum_conversion = {
            NviSensorType.NOT_DEFINED: SensorQualityIndicator.FIX_NOT_AVAILABLE,
            NviSensorType.GPS: SensorQualityIndicator.GPS_FIX,
            NviSensorType.GPS_DIFF: SensorQualityIndicator.DIFFERENTIAL_GPS_FIX,
            NviSensorType.RTK_FLOAT: SensorQualityIndicator.FLOAT_RTK,
        }
        for v1_enum, v2_enum in enum_conversion.items():
            result = np.where(
                nvi_sensor_types == v1_enum.numerator, v2_enum.numerator, result
            )

        return result

    def get_raw_quality_flags(self) -> np.ndarray:
        """
        @return: byte array of quality flags as written in the file variable "mbPFlag".
        """
        flag_variable = self.dataset.variables[FLAG]
        flag_variable.set_auto_chartostring(
            False
        )  # disable char to string autoconversion
        flag_variable.set_auto_maskandscale(
            False
        )  # do not try to compare byte to invalid value as int
        validity_flag = flag_variable[:]  # read the flags
        # no buffer conversion np.frombuffer(types_var[:], dtype=np.uint8)
        return validity_flag

    def get_quality_flags(self) -> np.ndarray:
        """
        Mapping of quality flags : from NVI legacy (.nvi) to NVI v2 (.nvi.nc).
        """
        # Convert byte flags to int flags
        raw_validity_flags = np.frombuffer(
            self.get_raw_quality_flags(), dtype=np.uint8
        ).astype(np.int32)

        result = np.full_like(
            raw_validity_flags,
            fill_value=QualityFlag.MISSING_VALUE.value,
            dtype=int,
        )

        enum_conversion = {
            NviQualityFlag.REMOVED: QualityFlag.MISSING_VALUE,
            NviQualityFlag.DOUBTFUL_UNVALIDATED: QualityFlag.PROBABLY_BAD_VALUE,
            NviQualityFlag.VALID_UNVALIDATED: QualityFlag.BAD_VALUE,
            NviQualityFlag.UNVALID: QualityFlag.BAD_VALUE,
            NviQualityFlag.MISSING: QualityFlag.MISSING_VALUE,
            NviQualityFlag.DOUBTFUL: QualityFlag.PROBABLY_GOOD_VALUE,
            NviQualityFlag.VALID: QualityFlag.GOOD_VALUE,
            NviQualityFlag.DOUBTFUL_VALIDATED: QualityFlag.GOOD_VALUE,
            NviQualityFlag.UNVALID_VALIDATED: QualityFlag.GOOD_VALUE,
            NviQualityFlag.REMOVED_VALIDATED: QualityFlag.MISSING_VALUE,
            NviQualityFlag.INTERPOLATED: QualityFlag.INTERPOLATED_VALUE,
            NviQualityFlag.MOVED: QualityFlag.CHANGED_VALUE,
            NviQualityFlag.SMOOTHED: QualityFlag.CHANGED_VALUE,
        }
        for v1_flag, v2_flag in enum_conversion.items():
            result = np.where(
                raw_validity_flags == v1_flag.value, v2_flag.value, result
            )

        return result

    def get_factor_of_quality_raw(self) -> np.ndarray:
        """return the factor_of_quality variable as written in the file"""
        quality_variable = self.dataset.variables[QUALITY]
        quality_variable.set_auto_chartostring(
            False
        )  # disable char to string autoconversion
        quality_variable.set_auto_maskandscale(
            False
        )  # do not try to compare byte to invalid value as int
        validity_flag = quality_variable[:]  # read the flags
        return validity_flag


@contextmanager
def nc_open(file_path: str, mode: str = "r") -> Generator[NviDriver, None, None]:
    """
    Define a With Statement Context Managers for a driver
    Allow opening a NviDriver in a With Statement
    """
    driver = NviDriver(file_path=file_path)
    driver.nc_open(mode)
    try:
        yield driver
    finally:
        driver.close()
