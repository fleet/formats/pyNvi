from enum import IntEnum


class QualityFlag(IntEnum):
    """
    NVI quality flags (store in /navigation/quality_flag variable).
    Details: see https://vocab.seadatanet.org/v_bodc_vocab_v2/search.asp?lib=L20
    """

    NO_QUALITY_CONTROL = 0
    GOOD_VALUE = 1
    PROBABLY_GOOD_VALUE = 2
    PROBABLY_BAD_VALUE = 3
    BAD_VALUE = 4
    CHANGED_VALUE = 5
    VALUE_BELOW_DETECTION = 6
    VALUE_IN_EXCESS = 7
    INTERPOLATED_VALUE = 8
    MISSING_VALUE = 9
