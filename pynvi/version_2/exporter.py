import logging
import os
from datetime import datetime
from pathlib import Path
from typing import List, NamedTuple, Tuple

import numpy as np

import pynvi.version_2.nvi_driver as nvi_d
from pynvi.version_2.nvi_groups import NavigationGrp as nav_g
from pynvi.legacy.exporter import Convert2Shp

_logger = logging.getLogger("Export NVI")


class ConvertNviV2ToShp(Convert2Shp):
    def get_navigation_data_values(
        self, filename
    ) -> Tuple[np.ndarray, np.ndarray, np.ndarray, datetime, datetime]:
        """
        Reads an NVI file, and fill the provided polyline and feature.
        """
        _logger.info(f"Input file (nvi) :{filename}")
        with nvi_d.nc_open(filename) as nvi:
            lon = nvi.get_longitudes()
            lat = nvi.get_latitudes()
            validity_flag = nvi.get_validities()

            start_date = nvi.get_start_date()
            end_date = nvi.get_end_date()

            return (lon, lat, validity_flag, start_date, end_date)


class ConvertToCsvArg(NamedTuple):
    """
    Class representing all arguments for configuring the conversion to CSV
    """

    i_paths: List[os.PathLike | str]
    o_paths: List[os.PathLike | str]
    header: bool = True
    separator: str = ";"
    other: str = ""
    elevation: str = "Positive"
    overwrite: bool = False


def convert_nvi_V2_to_csv(**kwargs) -> None:
    """
    Function accepting all arguments of the conversion as a dict. Possible arguments are listed in "ConvertToCsvArg" class
    """
    convert_nvi_V2_to_csv_with_args(ConvertToCsvArg(**kwargs))


def convert_nvi_V2_to_csv_with_args(args: ConvertToCsvArg):
    for i_path, o_path in zip(args.i_paths, args.o_paths):
        if isinstance(i_path, str):
            i_path = Path(i_path)
        if isinstance(o_path, str):
            o_path = Path(o_path)

        if not o_path.exists() or args.overwrite:
            _convert_nvi_V2_to_csv(i_path, o_path, args)
        else:
            _logger.warning(f"{o_path.name} exists and cannot be overwritten")


def _convert_nvi_V2_to_csv(
    i_path: os.PathLike, o_path: os.PathLike, args: ConvertToCsvArg
):
    separator = ";"
    if args.separator == "Other":
        separator = args.other
    else:
        separators = {"Semicolon": ";", "Comma": ",", "Space": " ", "Tabulation": "\t"}
        if args.separator in separators:
            separator = separators[args.separator]
        else:
            _logger.warning(
                f"Unexpected separator {args.separator}. Using default (semicolon)"
            )

    with nvi_d.xarray_open(i_path) as dataset:
        if args.elevation == "Negative":
            dataset[nav_g.VERTICAL_OFFSET_VNAME].values = -dataset[
                nav_g.VERTICAL_OFFSET_VNAME
            ].values

        df = dataset.to_pandas()
        # datetime in ISO 8601 format with milliseconds precision, Z is added to specify UTC
        df.index = df.index.map(lambda dt: dt.isoformat(timespec="milliseconds") + "Z")
        df.to_csv(
            o_path,
            sep=separator,
            columns=[
                nav_g.LATITUDE_VNAME,
                nav_g.LONGITUDE_VNAME,
                nav_g.HEIGHT_ABOVE_REFERENCE_ELLIPSOID_VNAME,
                nav_g.VERTICAL_OFFSET_VNAME,
                nav_g.SPEED_OVER_GROUND_VNAME,
                nav_g.HEADING_VNAME,
            ],
            header=(
                False
                if not args.header
                else [
                    "Latitude (degrees)",
                    "Longitude (degrees)",
                    "Height above ellipsoid (m)",
                    "Elevation (m)",
                    "Vessel speed (m/s)",
                    "Ship heading (degrees)",
                ]
            ),
            index_label=["Date"],
        )
