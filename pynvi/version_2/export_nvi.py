#! /usr/bin/env python3
# coding: utf-8

import logging
import os
from typing import Callable, List, NamedTuple

import numpy as np

from pynvi.version_2 import nvi_driver

__logger = logging.getLogger("Export NVI")

# pylint: disable=unsupported-binary-operation


class ExportNviArg(NamedTuple):
    """
    Class representing all arguments for configuring the process
    """

    # Path of Nvi file to create
    o_path: str

    # Mandatory layers
    time: np.ndarray
    latitude: np.ndarray
    longitude: np.ndarray
    heading: np.ndarray
    height_above_reference_ellipsoid: np.ndarray
    vertical_offset: np.ndarray

    # Optional layers
    source_filenames: List[str] = []
    pitch: np.ndarray | None = None
    roll: np.ndarray | None = None
    heading_rate: np.ndarray | None = None
    quality_flag: np.ndarray | None = None
    speed_over_ground: np.ndarray | None = None
    course_over_ground: np.ndarray | None = None
    pitch_rate: np.ndarray | None = None
    roll_rate: np.ndarray | None = None
    speed_relative: np.ndarray | None = None
    position_quality_indicator: np.ndarray | None = None

    overwrite: bool = False


def exports(**kwargs) -> None:
    """
    Function accepting all arguments of the process as a dict. Possible arguments are listed in "ExportNviArg" class
    """
    exports_with_ExportNviArg(ExportNviArg(**kwargs))


def exports_with_ExportNviArg(args: ExportNviArg) -> None:
    """
    Main function
    Use arguments to create a NVI file
    """
    if not os.path.exists(args.o_path) or args.overwrite:
        _process_export(args)
    else:
        __logger.warning(f"{args.o_path} exists and cannot be overwritten")


def _process_export(args: ExportNviArg) -> None:
    __logger.info(f"Exporting {args.o_path} ")

    with nvi_driver.nc_open(args.o_path, "w") as o_nvi:
        if args.source_filenames:
            o_nvi.create_source_filenames_var(args.source_filenames)

        # Mandatory variables
        _creates_variable(o_nvi.create_time_var, args.time)
        _creates_variable(o_nvi.create_latitude_var, args.latitude)
        _creates_variable(o_nvi.create_longitude_var, args.longitude)
        _creates_variable(o_nvi.create_heading_var, args.heading)
        _creates_variable(
            o_nvi.create_height_above_reference_ellipsoid_var,
            args.height_above_reference_ellipsoid,
        )
        _creates_variable(
            o_nvi.create_vertical_offset_var,
            args.vertical_offset,
        )
        _creates_variable(o_nvi.create_quality_flag_var, args.quality_flag)
        _creates_variable(o_nvi.create_heading_rate_var, args.heading_rate)
        _creates_variable(o_nvi.create_pitch_var, args.pitch)
        _creates_variable(o_nvi.create_roll_var, args.roll)

        # Optional variables
        _creates_variable(
            o_nvi.create_speed_over_ground_var,
            args.speed_over_ground,
            optional=True,
        )
        _creates_variable(
            o_nvi.create_course_over_ground_var,
            args.course_over_ground,
            optional=True,
        )
        _creates_variable(
            o_nvi.create_pitch_rate_var,
            args.pitch_rate,
            optional=True,
        )
        _creates_variable(
            o_nvi.create_roll_rate_var,
            args.roll_rate,
            optional=True,
        )
        _creates_variable(
            o_nvi.create_speed_relative_var,
            args.speed_relative,
            optional=True,
        )
        _creates_variable(
            o_nvi.create_position_quality_indicator_var,
            args.position_quality_indicator,
            optional=True,
        )


def _creates_variable(
    create_function: Callable,
    values: np.ndarray | None,
    optional: bool = False,
):
    # Creates an empty variable
    if values is not None or not optional:
        create_function(values)
