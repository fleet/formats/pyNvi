#! /usr/bin/env python3
# coding: utf-8

from contextlib import contextmanager
from datetime import datetime
from enum import Enum
from os import PathLike
from typing import Callable, Generator, List, Optional

import netCDF4 as nc
import numpy as np
import xarray as xa
from numpy.typing import ArrayLike

import pynvi.version_2.nvi_groups as nvi_g
from pynvi.abstract_nvi_driver import AbstractNviDriver


# pylint: disable=unsupported-binary-operation,too-many-public-methods,unsupported-membership-test


class SensorQualityIndicator(int, Enum):
    FIX_NOT_AVAILABLE = (0, "fix not available")
    GPS_FIX = (1, "GPS fix")
    DIFFERENTIAL_GPS_FIX = (2, "Differential GPS fix")
    PPS_FIX = (3, "PPS fix")
    REAL_TIME_KINEMATIC = (4, "Real Time Kinematic")
    FLOAT_RTK = (5, "Float RTK")
    ESTIMATED = (6, "estimated (dead reckoning)")
    MANUAL_INPUT_MODE = (7, "Manual input mode")
    SIMULATION_MODE = (8, "Simulation mode")

    def __new__(cls, value, label):
        obj = int.__new__(cls, value)
        obj._value_ = value
        obj.label = label
        return obj


class NviDriver(AbstractNviDriver):
    def __init__(self, file_path: PathLike | str):
        self.file_path = file_path
        self._dataset = None

    def nc_open(self, mode: str = "r") -> nc.Dataset:
        """
        Open the file and return the resulting Dataset
        """
        self._dataset = nc.Dataset(self.file_path, mode, format="NETCDF4")
        if mode in ["w", "x"]:
            root_g = nvi_g.RootGrp()
            root_g.create_group(self._dataset)

        return self._dataset

    def get_file_path(self) -> PathLike | str | None:
        return self.file_path

    def create_provenance_grp(self, **attributes) -> nc.Group:
        """Add a group /Provenance"""
        provenance_path = nvi_g.ProvenanceGrp.get_group_path()
        if provenance_path not in self._dataset.groups:
            provenance_g = nvi_g.ProvenanceGrp()
            return provenance_g.create_group(self._dataset, **attributes)
        return self._dataset.groups[provenance_path]

    def create_navigation_grp(self, **attributes) -> nc.Group:
        """Add a group /Navigation"""
        navigation_path = nvi_g.NavigationGrp.get_group_path()
        if navigation_path not in self._dataset.groups:
            navigation_g = nvi_g.NavigationGrp()
            return navigation_g.create_group(self._dataset, **attributes)
        return self._dataset.groups[navigation_path]

    def create_platform_grp(self, **attributes) -> nc.Group:
        """Add a group /Platform"""
        platform_path = nvi_g.PlatformGrp.get_group_path()
        if platform_path not in self._dataset.groups:
            platform_g = nvi_g.PlatformGrp()
            return platform_g.create_group(self._dataset, **attributes)
        return self._dataset.groups[platform_path]

    def create_position_grp(self, **attributes) -> nc.Group:
        """Add a group /Platform/Position"""
        self.create_platform_grp()

        position_path = nvi_g.PositionGrp.get_group_path()
        if position_path not in self._dataset.groups:
            position_g = nvi_g.PositionGrp()
            return position_g.create_group(self._dataset, **attributes)

        return self._dataset.groups[position_path]

    def create_attitude_grp(self, **attributes) -> nc.Group:
        """Add a group /Platform/Attitude"""
        self.create_platform_grp()

        attitude_path = nvi_g.AttitudeGrp.get_group_path()
        if attitude_path not in self._dataset.groups:
            attitude_g = nvi_g.AttitudeGrp()
            return attitude_g.create_group(self._dataset, **attributes)

        return self._dataset.groups[attitude_path]

    def close(self):
        """
        Close the file
        """
        if self._dataset is not None:
            self._dataset.close()
            self._dataset = None

    def create_source_filenames_var(self, filenames: List[str]) -> nc.Variable:
        """
        Add and fill the filename variable
        """
        nc_provenance_grp = self.create_provenance_grp()

        # Creates dimension
        provenance_g = nvi_g.ProvenanceGrp()
        provenance_g.create_dimension(
            group=nc_provenance_grp,
            values={nvi_g.ProvenanceGrp.FILENAMES_DIM_NAME: len(filenames)},
        )

        # Creates variables
        result = provenance_g.create_source_filenames(group=nc_provenance_grp)
        for i, filename in enumerate(filenames):
            result[i] = filename

        return result

    def create_time_var(self, times: ArrayLike, **attributes) -> nc.Variable:
        """
        Add and fill the time variable
        """
        navigation_g = nvi_g.NavigationGrp()
        return self._create_one_navigation_var(
            create_method=navigation_g.create_time, values=times, **attributes
        )

    def create_latitude_var(self, latitudes: ArrayLike, **attributes) -> nc.Variable:
        """
        Add and fill the latitude variable
        """
        navigation_g = nvi_g.NavigationGrp()
        return self._create_one_navigation_var(
            create_method=navigation_g.create_latitude, values=latitudes, **attributes
        )

    def create_longitude_var(self, longitudes: ArrayLike, **attributes) -> nc.Variable:
        """
        Add and fill the longitude variable
        """
        navigation_g = nvi_g.NavigationGrp()
        return self._create_one_navigation_var(
            create_method=navigation_g.create_longitude, values=longitudes, **attributes
        )

    def create_heading_var(self, headings: ArrayLike, **attributes) -> nc.Variable:
        """
        Add and fill the heading variable
        """
        navigation_g = nvi_g.NavigationGrp()
        return self._create_one_navigation_var(
            create_method=navigation_g.create_heading, values=headings, **attributes
        )

    def create_pitch_var(
        self, pitchs: ArrayLike | None = None, **attributes
    ) -> nc.Variable:
        """
        Add and fill the pitch variable
        """
        navigation_g = nvi_g.NavigationGrp()
        return self._create_one_navigation_var(
            create_method=navigation_g.create_pitch, values=pitchs, **attributes
        )

    def create_roll_var(
        self, rolls: ArrayLike | None = None, **attributes
    ) -> nc.Variable:
        """
        Add and fill the roll variable
        """
        navigation_g = nvi_g.NavigationGrp()
        return self._create_one_navigation_var(
            create_method=navigation_g.create_roll, values=rolls, **attributes
        )

    def create_quality_flag_var(
        self, quality_flags: ArrayLike, **attributes
    ) -> nc.Variable:
        """
        Add and fill the quality_flag variable
        """
        navigation_g = nvi_g.NavigationGrp()
        return self._create_one_navigation_var(
            create_method=navigation_g.create_quality_flag,
            values=quality_flags,
            **attributes,
        )

    def create_heading_rate_var(
        self, heading_rates: ArrayLike | None = None, **attributes
    ) -> nc.Variable:
        """
        Add and fill the heading_rate variable
        """
        navigation_g = nvi_g.NavigationGrp()
        return self._create_one_navigation_var(
            create_method=navigation_g.create_heading_rate,
            values=heading_rates,
            **attributes,
        )

    def create_course_over_ground_var(
        self, course_over_grounds: ArrayLike, **attributes
    ) -> nc.Variable:
        """
        Add and fill the course_over_ground variable
        """
        navigation_g = nvi_g.NavigationGrp()
        return self._create_one_navigation_var(
            create_method=navigation_g.create_course_over_ground,
            values=course_over_grounds,
            **attributes,
        )

    def create_pitch_rate_var(
        self, pitch_rates: ArrayLike, **attributes
    ) -> nc.Variable:
        """
        Add and fill the pitch_rate variable
        """
        navigation_g = nvi_g.NavigationGrp()
        return self._create_one_navigation_var(
            create_method=navigation_g.create_pitch_rate,
            values=pitch_rates,
            **attributes,
        )

    def create_roll_rate_var(self, roll_rates: ArrayLike, **attributes) -> nc.Variable:
        """
        Add and fill the roll_rate variable
        """
        navigation_g = nvi_g.NavigationGrp()
        return self._create_one_navigation_var(
            create_method=navigation_g.create_roll_rate, values=roll_rates, **attributes
        )

    def create_speed_over_ground_var(
        self, speed_over_grounds: ArrayLike, **attributes
    ) -> nc.Variable:
        """
        Add and fill the speed_over_ground variable
        """
        navigation_g = nvi_g.NavigationGrp()
        return self._create_one_navigation_var(
            create_method=navigation_g.create_speed_over_ground,
            values=speed_over_grounds,
            **attributes,
        )

    def create_speed_relative_var(
        self, speed_relatives: ArrayLike, **attributes
    ) -> nc.Variable:
        """
        Add and fill the speed_relative variable
        """
        navigation_g = nvi_g.NavigationGrp()
        return self._create_one_navigation_var(
            create_method=navigation_g.create_speed_relative,
            values=speed_relatives,
            **attributes,
        )

    def create_height_above_reference_ellipsoid_var(
        self, height_above_reference_ellipsoids: ArrayLike, **attributes
    ) -> nc.Variable:
        """
        Add and fill the height_above_reference_ellipsoid variable
        """
        navigation_g = nvi_g.NavigationGrp()
        return self._create_one_navigation_var(
            create_method=navigation_g.create_height_above_reference_ellipsoid,
            values=height_above_reference_ellipsoids,
            **attributes,
        )

    def create_position_quality_indicator_var(
        self, position_quality_indicators: ArrayLike, **attributes
    ) -> nc.Variable:
        """
        Add and fill the position_quality_indicator variable
        """
        navigation_g = nvi_g.NavigationGrp()
        return self._create_one_navigation_var(
            create_method=navigation_g.create_position_quality_indicator,
            values=position_quality_indicators,
            **attributes,
        )

    def create_position_sub_group(self, ident: str, **attributes) -> nc.Variable:
        """
        Add a group /Platform/Position/{ident}
        """
        nc_position_grp = self.create_position_grp()

        position_sub_g = nvi_g.PositionSubGroup()
        position_sub_g.create_group(
            parent_group=nc_position_grp, ident=ident, **attributes
        )

    def create_attitude_sub_group(self, ident: str, **attributes) -> nc.Variable:
        """
        Add a group /Platform/Attitude/{ident}
        """
        nc_attitude_grp = self.create_attitude_grp()

        attitude_sub_g = nvi_g.AttitudeSubGroup()
        attitude_sub_g.create_group(
            parent_group=nc_attitude_grp, ident=ident, **attributes
        )

    def create_vertical_offset_var(
        self, vertical_offsets: ArrayLike, **attributes
    ) -> nc.Variable:
        """
        Add and fill the vertical_offset variable
        """
        navigation_g = nvi_g.NavigationGrp()
        return self._create_one_navigation_var(
            create_method=navigation_g.create_vertical_offset,
            values=vertical_offsets,
            **attributes,
        )

    def get_data(self, variable: str) -> np.ndarray:
        """
        Returns data of the specified variable (from navigation group).
        """
        return self._dataset[nvi_g.NavigationGrp.get_group_path()].variables[variable][
            :
        ]

    def get_times(self) -> np.ndarray:
        return (
            self._dataset[nvi_g.NavigationGrp.get_group_path()]
            .variables[nvi_g.NavigationGrp.TIME_VNAME][:]
            .astype("datetime64[ns]")
        )

    @staticmethod
    def __get_utc_date(date: np.datetime64) -> datetime:
        """
        Converts datetime64 to UTC datetime
        """
        epoch = np.datetime64(0, "s")
        one_second = np.timedelta64(1, "s")
        return datetime.utcfromtimestamp((date - epoch) / one_second)

    def get_start_date(self) -> datetime:
        return self.__get_utc_date(self.get_times()[0])

    def get_end_date(self) -> datetime:
        return self.__get_utc_date(self.get_times()[-1])

    def get_latitudes(self) -> np.ndarray:
        return self.get_data(nvi_g.NavigationGrp.LATITUDE_VNAME)

    def get_longitudes(self) -> np.ndarray:
        return self.get_data(nvi_g.NavigationGrp.LONGITUDE_VNAME)

    def get_altitudes(self) -> np.ndarray:
        return self.get_data(nvi_g.NavigationGrp.HEIGHT_ABOVE_REFERENCE_ELLIPSOID_VNAME)

    def get_speeds(self) -> Optional[np.ndarray]:
        return (
            self.get_data(nvi_g.NavigationGrp.SPEED_OVER_GROUND_VNAME)
            if self.has_speed_over_ground()
            else None
        )

    def get_headings(self) -> np.ndarray:
        return self.get_data(nvi_g.NavigationGrp.HEADING_VNAME)

    def get_sensor_quality_indicators(self) -> np.ndarray:
        return self.get_data(nvi_g.NavigationGrp.POSITION_QUALITY_INDICATOR_VNAME)

    def get_quality_flags(self) -> np.ndarray:
        """
        @return: array of Quality Flags (see : QualityFlag enum).
        """
        return self.get_data(nvi_g.NavigationGrp.QUALITY_FLAG_VNAME)

    def get_courses_over_ground(self) -> Optional[np.ndarray]:
        return (
            self.get_data(nvi_g.NavigationGrp.COURSE_OVER_GROUND_VNAME)
            if self.has_course_over_ground()
            else None
        )

    def has_course_over_ground(self) -> bool:
        """
        Return true if file contains the course_over_ground variable
        """
        return self._has_one_navigation_var(
            nvi_g.NavigationGrp.COURSE_OVER_GROUND_VNAME
        )

    def has_pitch_rate(self) -> bool:
        """
        Return true if file contains the pitch_rate variable
        """
        return self._has_one_navigation_var(nvi_g.NavigationGrp.PITCH_RATE_VNAME)

    def has_roll_rate(self) -> bool:
        """
        Return true if file contains the roll_rate variable
        """
        return self._has_one_navigation_var(nvi_g.NavigationGrp.ROLL_RATE_VNAME)

    def has_speed_over_ground(self) -> bool:
        """
        Return true if file contains the speed_over_ground variable
        """
        return self._has_one_navigation_var(nvi_g.NavigationGrp.SPEED_OVER_GROUND_VNAME)

    def has_speed_relative(self) -> bool:
        """
        Return true if file contains the speed_relative variable
        """
        return self._has_one_navigation_var(nvi_g.NavigationGrp.SPEED_RELATIVE_VNAME)

    def _create_one_navigation_var(
        self, create_method: Callable, values: ArrayLike | None, **attributes
    ) -> nc.Variable:
        """
        Add and fill the one of the navigation variable
        """
        nc_navigation_grp = self.create_navigation_grp()

        # Creates dimension
        navigation_g = nvi_g.NavigationGrp()
        if nvi_g.NavigationGrp.TIME_DIM_NAME not in nc_navigation_grp.dimensions:
            navigation_g.create_dimension(
                group=nc_navigation_grp,
                values={nvi_g.NavigationGrp.TIME_DIM_NAME: len(values)},
            )

        # Creates variables
        result = create_method(group=nc_navigation_grp, **attributes)
        if values is not None:
            result[:] = values

        return result

    def _has_one_navigation_var(self, var_name: str) -> bool:
        """
        Return true if file contains the specified navigation variable
        """
        try:
            return (
                var_name
                in self._dataset[nvi_g.NavigationGrp.get_group_path()].variables
            )
        except IndexError:
            return False


@contextmanager
def nc_open(
    file_path: PathLike | str, mode: str = "r"
) -> Generator[NviDriver, None, None]:
    """
    Define a With Statement Context Managers for a NviDriver
    Allow opening a NviDriver in a With Statement
    """
    driver = NviDriver(file_path)
    driver.nc_open(mode)
    try:
        yield driver
    finally:
        driver.close()


@contextmanager
def xarray_open(file_path: PathLike | str) -> Generator[xa.Dataset, None, None]:
    """
    Open the NVI file with XArray lib.
    Time coordinate is in a DataArray of numpy.datetime64
    """
    result = None
    try:
        result = xa.open_dataset(file_path, group=nvi_g.NavigationGrp.get_group_path())
        yield result
    finally:
        if result is not None:
            result.close()
