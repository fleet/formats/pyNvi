#! /usr/bin/env python3
# coding: utf-8

import logging
import os
from pathlib import Path
from typing import List, NamedTuple

import numpy as np

import pynvi.legacy.nvi_driver as driver_v1
import pynvi.version_2.nvi_driver as driver_v2

__logger = logging.getLogger("Upgrade NVI")


class UpgradeArg(NamedTuple):
    """
    Class representing all arguments for configuring the process
    """

    i_paths: List[os.PathLike | str]
    o_paths: List[os.PathLike | str]
    overwrite: bool = False


def upgrades(**kwargs) -> None:
    """
    Function accepting all arguments of the process as a dict. Possible arguments are listed in "UpgradeArg" class
    """
    upgrades_with_args(UpgradeArg(**kwargs))


def upgrades_with_args(args: UpgradeArg) -> None:
    """
    Main function

    Browse the list of i_paths and upgrade each file
    """
    for i_path, o_path in zip(args.i_paths, args.o_paths):
        if isinstance(i_path, str):
            i_path = Path(i_path)
        if isinstance(o_path, str):
            o_path = Path(o_path)

        if not o_path.exists() or args.overwrite:
            _upgrades_one_file(i_path, o_path)
        else:
            __logger.warning(f"{o_path.name} exists and cannot be overwritten")


def _upgrades_one_file(i_path: os.PathLike, o_path: os.PathLike) -> None:
    """Upgrades i_path NVI file to o_path"""
    with driver_v1.nc_open(str(i_path)) as i_nvi, driver_v2.nc_open(
        o_path, "w"
    ) as o_nvi:
        o_nvi.create_source_filenames_var([str(i_path)])

        # Convert time in milliseconds to nanoseconds
        time_in_ns = i_nvi.get_times().astype(np.uint64) * 10**6
        o_nvi.create_time_var(times=time_in_ns)

        o_nvi.create_latitude_var(latitudes=i_nvi.get_latitudes())
        o_nvi.create_longitude_var(longitudes=i_nvi.get_longitudes())
        o_nvi.create_heading_var(headings=i_nvi.get_headings())
        o_nvi.create_height_above_reference_ellipsoid_var(
            height_above_reference_ellipsoids=i_nvi.get_altitudes()
        )
        o_nvi.create_vertical_offset_var(vertical_offsets=-i_nvi.get_immersion())
        o_nvi.create_quality_flag_var(quality_flags=i_nvi.get_quality_flags())
        o_nvi.create_position_quality_indicator_var(
            position_quality_indicators=i_nvi.get_sensor_quality_indicators()
        )
        o_nvi.create_speed_over_ground_var(speed_over_grounds=i_nvi.get_speeds())

        # Mandatory variables with no values in legacy nvi
        o_nvi.create_heading_rate_var()
        o_nvi.create_pitch_var()
        o_nvi.create_roll_var()
