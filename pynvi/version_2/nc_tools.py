import netCDF4 as nc


def find_type(group: nc.Dataset, name: str):
    """
    Recurse upward group hierarchy to find the type definition
    """
    if hasattr(group, "vltypes") and name in group.vltypes:
        return group.vltypes[name]
    if hasattr(group, "enumtypes") and name in group.enumtypes:
        return group.enumtypes[name]
    if hasattr(group, "cmptypes") and name in group.cmptypes:
        return group.cmptypes[name]

    if hasattr(group, "parent"):
        # recurse to find attribute definition in parent group
        return find_type(group.parent, name)
    raise AttributeError(f"group hierarchy has no {name} Vlen type definition found")
