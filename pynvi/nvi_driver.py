from contextlib import contextmanager
from typing import Generator

from pynvi.abstract_nvi_driver import AbstractNviDriver
from pynvi.filtered_nvi_driver import FilteredNviDriver
from pynvi.legacy.nvi_driver import NviDriver
from pynvi.version_2.nvi_driver import NviDriver as NviDriverV2


@contextmanager
def open_nvi(
    file_path: str, mode: str = "r", filtered: bool = False
) -> Generator[AbstractNviDriver, None, None]:
    """
    Define a "With" Statement Context Managers for a NviDriver.
    (@contextmanager to allow opening a NviDriver in a "with... as..." Statement.)

    @param mode: access mode (see NetCDF documentation for details).
    @param file_path: path of the NVI file (.nvi or .nvi.nc).
    @param filtered: default = False, if True, return a driver with access to only valid data.
    """
    driver = get_nvi_driver(file_path, filtered)
    driver.nc_open(mode)
    try:
        yield driver
    finally:
        driver.close()


def get_nvi_driver(file_path: str, filtered: bool = False) -> AbstractNviDriver:
    """
    Instantiates the suitable NVI driver for the specified file.

    @param file_path: path of the NVI file (.nvi or .nvi.nc).
    @param filtered: default = False, if True, return a driver with access to only valid data.
    @return: NVI driver.
    """
    if file_path.endswith(".nvi"):
        return (
            FilteredNviDriver(NviDriver(file_path))
            if filtered
            else NviDriver(file_path)
        )
    elif file_path.endswith(".nvi.nc"):
        return (
            FilteredNviDriver(NviDriverV2(file_path))
            if filtered
            else NviDriverV2(file_path)
        )

    raise ValueError(f"Unsupported NVI file : {file_path}.")
