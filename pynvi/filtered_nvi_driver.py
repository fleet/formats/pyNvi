from os import PathLike
from typing import Optional

import netCDF4 as nc
import numpy as np

from pynvi.abstract_nvi_driver import AbstractNviDriver


class FilteredNviDriver(AbstractNviDriver):
    """
    Wrapper of AbstractNviDriver to get access to only valid data.
    """

    driver: AbstractNviDriver
    valid_mask: np.ndarray

    def __init__(self, driver: AbstractNviDriver):
        self.driver = driver

    def nc_open(self, mode: str = "r") -> nc.Dataset:
        dataset = self.driver.nc_open()
        self.valid_mask = self.driver.get_validities()
        return dataset

    def close(self):
        self.driver.close()

    def get_name(self) -> str:
        return self.driver.get_name()

    def get_file_path(self) -> PathLike | str | None:
        return self.driver.get_file_path()

    def get_times(self) -> np.ndarray:
        return self.driver.get_times()[self.valid_mask]

    def get_latitudes(self) -> np.ndarray:
        return self.driver.get_latitudes()[self.valid_mask]

    def get_longitudes(self) -> np.ndarray:
        return self.driver.get_longitudes()[self.valid_mask]

    def get_headings(self) -> np.ndarray:
        return self.driver.get_headings()[self.valid_mask]

    def get_altitudes(self) -> Optional[np.ndarray]:
        return self.driver.get_altitudes()[self.valid_mask]

    def get_speeds(self) -> Optional[np.ndarray]:
        return self.driver.get_speeds()[self.valid_mask]

    def get_courses_over_ground(self) -> Optional[np.ndarray]:
        return self.driver.get_courses_over_ground()[self.valid_mask]

    def get_quality_flags(self) -> np.ndarray:
        return self.driver.get_quality_flags()[self.valid_mask]

    def get_validities(self) -> Optional[np.ndarray]:
        return self.valid_mask

    def get_sensor_quality_indicators(self) -> Optional[np.ndarray]:
        return self.driver.get_sensor_quality_indicators()[self.valid_mask]
