import os.path
from abc import abstractmethod
from os import PathLike
from typing import runtime_checkable, Protocol, Optional

import netCDF4 as nc
import numpy as np

from pynvi.version_2.quality_flag import QualityFlag


@runtime_checkable
class AbstractNviDriver(Protocol):
    """
    API description of NVI driver.

    WARNING : this protocol shall match the AbstractNavigation protocol of PyAT project.
    """

    @abstractmethod
    def nc_open(self, mode: str = "r") -> nc.Dataset: ...

    @abstractmethod
    def close(self): ...

    def get_name(self) -> str:
        return os.path.basename(self.get_file_path())

    @abstractmethod
    def get_file_path(self) -> PathLike | str | None: ...

    @abstractmethod
    def get_times(self) -> np.ndarray: ...

    @abstractmethod
    def get_latitudes(self) -> np.ndarray: ...

    @abstractmethod
    def get_longitudes(self) -> np.ndarray: ...

    @abstractmethod
    def get_headings(self) -> np.ndarray: ...

    @abstractmethod
    def get_altitudes(self) -> Optional[np.ndarray]: ...

    @abstractmethod
    def get_speeds(self) -> Optional[np.ndarray]: ...

    @abstractmethod
    def get_courses_over_ground(self) -> Optional[np.ndarray]: ...

    @abstractmethod
    def get_quality_flags(self) -> np.ndarray:
        """
        Returns an array of NVI quality flag values.
        Details: see https://vocab.seadatanet.org/v_bodc_vocab_v2/search.asp?lib=L20
        """

    def get_validities(self) -> np.ndarray:
        """
        Returns an array of boolean values, with True for valid data (computed from quality flags).
        """
        quality_flags = self.get_quality_flags()

        # Quality flag considered valid
        valid_flags = {
            QualityFlag.NO_QUALITY_CONTROL,
            QualityFlag.GOOD_VALUE,
            QualityFlag.PROBABLY_GOOD_VALUE,
            QualityFlag.CHANGED_VALUE,
            QualityFlag.INTERPOLATED_VALUE,
        }

        return np.isin(quality_flags, np.array([flag.value for flag in valid_flags]))

    @abstractmethod
    def get_sensor_quality_indicators(self) -> Optional[np.ndarray]:
        """
        Returns Sensor Quality Indicators, see enum SensorQualityIndicator (version_2/nvi_driver).
        """
