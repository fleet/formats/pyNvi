pipeline {
	agent any
	stages {
        stage('Pre-build') {
            steps {
                script {
                    currentBuild.displayName = "#${env.BUILD_NUMBER} ${env.GIT_BRANCH}"
                }
            }
        }
		stage('Create environment') {
			steps {
				echo 'Building anaconda environment'
				echo 'Path = ${Path}'
                bat label: 'create conda environments', script: '''call C:\\Tools\\Miniconda3\\Library\\bin\\conda.bat activate
							cd requirements
							create_env_windows.bat
							cd ..
							'''
			}
		}
		stage('test'){
			steps{
				bat label: 'cleaning python caches', script: '''call C:\\Tools\\Miniconda3\\Library\\bin\\conda.bat activate pynvi_env
                            git reset --hard
                            git clean -fx
                            '''
				bat label: 'Running pylint on core package', script: '''call C:\\Tools\\Miniconda3\\Library\\bin\\conda.bat activate pynvi_env
							pylint pynvi
							'''
				bat label: 'Running pytest', script: '''call C:\\Tools\\Miniconda3\\Library\\bin\\conda.bat activate pynvi_env
							python.exe -m pytest --verbose --junit-xml test-reports/results.xml -o junit_family=xunit2 --maxfail=150
							'''
			}
		}
    }
	post {
        always {
            // Archive unit tests for the future
            junit allowEmptyResults: true, testResults: 'pynvi/test-reports/results.xml'
        }
        failure {
            emailext (
            subject: "Jenkins FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}] ${env.GIT_BRANCH}'",
            attachLog: true,
            body: """BUILD FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}] ${env.GIT_BRANCH}': Check console output at ${env.BUILD_URL}
                (see Log attached for details)
            """,
            to: 'cyrille.poncelet@ifremer.fr, gael.billant@ifremer.fr, Anthony.Saunier@ifremer.fr',
            recipientProviders: [[$class: 'DevelopersRecipientProvider']]

            )
        }
        success {
            emailext (
                subject: "Jenkins Pyat SUCCESS: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}] ${env.GIT_BRANCH}'",
                attachLog: true,
                body: """BUILD SUCCESS: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}] ${env.GIT_BRANCH}'
                    Check console output at ${env.BUILD_URL}
                    Download miniconda : ${env.BUILD_URL}artifact/miniconda.zip
                """,
                to: 'cyrille.poncelet@ifremer.fr, gael.billant@ifremer.fr, Anthony.Saunier@ifremer.fr',
                recipientProviders: [[$class: 'DevelopersRecipientProvider']]
                )
        }
    }
}
