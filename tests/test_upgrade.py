import tempfile as tmp
from pathlib import Path

import numpy as np
from numpy.testing import assert_almost_equal

import pynvi.legacy.nvi_driver as nvi_legacy
import pynvi.version_2.nvi_driver as nvi_v2
import pynvi.version_2.upgrade_nvi as upgrade_nvi
from .file_installer import get_test_path

NVI_PATH = get_test_path().absolute() / "nvi" / "0136_20120607_083636_ShipName_ref.nvi"


def test_upgrade():
    """
    Test the upgrade of a NVI file
    """
    with tmp.TemporaryDirectory() as temp_dir:
        o_path = Path(temp_dir, NVI_PATH.name.replace(".nvi", ".nvi.nc"))
        upgrade_nvi.upgrades(i_paths=[NVI_PATH], o_paths=[o_path])

        with nvi_v2.xarray_open(o_path) as dataset:
            print(dataset)
            # Dimensions
            assert dataset.dims["time"] == 1176

            # Coordinate variables
            first_date = dataset.coords["time"].values[0]
            assert first_date == np.datetime64("2012-06-07 08:36:35.328")

            assert_almost_equal(
                dataset.coords["latitude"].values[0], 43.6727, decimal=4
            )
            assert_almost_equal(
                dataset.coords["longitude"].values[0], -1.5653, decimal=4
            )

            # Variables
            assert_almost_equal(
                dataset.data_vars["heading"].values[0], 92.01, decimal=2
            )
            assert dataset.data_vars["position_quality_indicator"].values[0] == 0
            assert dataset.data_vars["quality_flag"].values[0] == 1

        # Global comparison with drivers
        with nvi_legacy.nc_open(NVI_PATH) as nvi_driver_legacy, nvi_v2.nc_open(
            o_path
        ) as nvi_driver_v2:
            compare_nvi_drivers(nvi_driver_legacy, nvi_driver_v2)


def compare_nvi_drivers(driver1: nvi_legacy, driver2: nvi_v2):
    """Compare legacy NVI and new NVI drivers."""

    # Compare data
    assert np.array_equal(
        driver1.get_times(), driver2.get_times()
    ), "Times do not match"
    assert np.array_equal(
        driver1.get_latitudes(), driver2.get_latitudes()
    ), "Latitudes do not match"
    assert np.array_equal(
        driver1.get_longitudes(), driver2.get_longitudes()
    ), "Longitudes do not match"
    assert np.allclose(
        driver1.get_headings(), driver2.get_headings()
    ), "Headings do not match"
    assert np.array_equal(
        driver1.get_quality_flags(), driver2.get_quality_flags()
    ), "Quality flags do not match"
    assert np.array_equal(
        driver1.get_validities(), driver2.get_validities()
    ), "Validities do not match"

    # Compare optional arrays
    assert (driver1.get_altitudes() is None) == (
        driver2.get_altitudes() is None
    ), "Altitude presence mismatch"
    if driver1.get_altitudes() is not None:
        assert np.allclose(
            driver1.get_altitudes(), driver2.get_altitudes()
        ), "Altitudes do not match"

    assert (driver1.get_speeds() is None) == (
        driver2.get_speeds() is None
    ), "Speeds presence mismatch"
    if driver1.get_speeds() is not None:
        assert np.array_equal(
            driver1.get_speeds(), driver2.get_speeds()
        ), "Speeds do not match"

    assert (driver1.get_courses_over_ground() is None) == (
        driver2.get_courses_over_ground() is None
    ), "Courses presence mismatch"
    if driver1.get_courses_over_ground() is not None:
        assert np.array_equal(
            driver1.get_courses_over_ground(), driver2.get_courses_over_ground()
        ), "Courses over ground do not match"

    assert (driver1.get_sensor_quality_indicators() is None) == (
        driver2.get_sensor_quality_indicators() is None
    ), "Sensor quality indicator presence mismatch"
    if driver1.get_sensor_quality_indicators() is not None:
        assert np.array_equal(
            driver1.get_sensor_quality_indicators(),
            driver2.get_sensor_quality_indicators(),
        ), "Sensor quality indicators do not match"
