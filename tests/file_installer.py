"""Download and install test files """
import os
import pathlib
import urllib.request as req
import zipfile
from pathlib import Path

#version release for test path
release = "0.0.1"

destination_dir = Path(__file__).parent / "data" #match subdirectory data under this path

project_url = f"https://gitlab.ifremer.fr/api/v4/projects/1830/packages/generic/pyat_test_file/{release}/pyat_test_file.zip"


def get_test_path()->Path:
    """Get test path, if does not exists tests will be unzipped and installed

        return the Path of test directory
    """
    unzip_dst = f"{destination_dir}/{release}/pyat_test_file"
    if not os.path.isdir(unzip_dst):
        print(f"Downloading test files")

        dst = f"{destination_dir}/pyat_test_file_{release}.zip"
        req.urlretrieve(project_url, dst)

        with zipfile.ZipFile(dst, 'r') as zip_ref:
            zip_ref.extractall(path=unzip_dst)

        os.remove(dst)
    unzip_dst = pathlib.Path(unzip_dst)
    print(f"Using tests file in {unzip_dst}")
    return unzip_dst
