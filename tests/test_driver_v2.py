import tempfile as tmp
from pathlib import Path

from numpy.testing import assert_almost_equal

import pynvi.legacy.nvi_driver as nvi_v1
import pynvi.version_2.nvi_driver as nvi_v2
import pynvi.version_2.upgrade_nvi as upgrade_nvi
from .file_installer import get_test_path

NVI_PATH = get_test_path().absolute() / "nvi" / "0136_20120607_083636_ShipName_ref.nvi"


def test_read_nvi_v2():
    """
    Upgrades a NVI legacy and compares the read functions
    """
    with tmp.TemporaryDirectory() as temp_dir:
        o_path = Path(temp_dir, "ShipName_v2.nvi")
        upgrade_nvi.upgrades(i_paths=[NVI_PATH], o_paths=[o_path])

        with nvi_v1.nc_open(NVI_PATH) as nvi_in, nvi_v2.nc_open(o_path) as nvi_out:
            assert nvi_in.get_times()[0] == nvi_out.get_times()[0]
            assert nvi_in.get_times()[-1] == nvi_out.get_times()[-1]
            print(nvi_in.get_start_date())
            print(nvi_out.get_start_date())
            assert nvi_in.get_start_date() == nvi_out.get_start_date()
            assert nvi_in.get_end_date() == nvi_out.get_end_date()

            assert nvi_in.get_latitudes()[0] == nvi_out.get_latitudes()[0]
            assert nvi_in.get_latitudes()[-1] == nvi_out.get_latitudes()[-1]

            assert nvi_in.get_longitudes()[0] == nvi_out.get_longitudes()[0]
            assert nvi_in.get_longitudes()[-1] == nvi_out.get_longitudes()[-1]

            assert_almost_equal(
                nvi_in.get_headings()[0], nvi_out.get_headings()[0], decimal=2
            )
            assert_almost_equal(
                nvi_in.get_headings()[-1], nvi_out.get_headings()[-1], decimal=2
            )

            assert_almost_equal(
                nvi_in.get_altitudes()[0], nvi_out.get_altitudes()[0], decimal=2
            )
            assert_almost_equal(
                nvi_in.get_altitudes()[-1], nvi_out.get_altitudes()[-1], decimal=2
            )

            assert_almost_equal(
                nvi_in.get_speeds()[0], nvi_out.get_speeds()[0], decimal=2
            )
            assert_almost_equal(
                nvi_in.get_speeds()[-1], nvi_out.get_speeds()[-1], decimal=2
            )

            assert nvi_in.get_sensor_types()[0] == nvi_out.get_sensor_quality_indicators()[0]
            assert nvi_in.get_sensor_types()[-1] == nvi_out.get_sensor_quality_indicators()[-1]
