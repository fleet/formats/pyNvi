import tempfile as tmp
from pathlib import Path

import numpy as np
from numpy.testing import assert_almost_equal

import pynvi.version_2.export_nvi as export_nvi
import pynvi.version_2.nvi_driver as nvi_v2

from .file_installer import get_test_path

NVI_PATH = get_test_path().absolute() / "nvi" / "0136_20120607_083636_ShipName_ref.nvi"


def test_export():
    """
    Test the export of a NVI file
    """
    with tmp.TemporaryDirectory() as temp_dir:
        times_in_ns = (
            np.array(
                [
                    np.datetime64("2012-06-07 08:36:35.328"),
                    np.datetime64("2012-06-07 08:36:36.444"),
                ],
                dtype=np.uint64,
            )
            * 10**6  # milli to nano
        )

        o_latitudes = np.array([43.01, 43.02])
        o_longitudes = np.array([-1.01, -1.02])
        o_headings = np.array([45.5, 46.1], dtype=np.float32)
        o_heading_rates = np.array([11.1, 22.2], dtype=np.float32)
        o_height_above_reference_ellipsoid = np.array([10.1, 10.2], dtype=np.float32)
        o_quality_flag = np.array([2, 1], dtype=np.uint8)
        o_pitchs = np.array([0.1, -0.1], dtype=np.float32)
        o_rolls = np.array([0.2, -0.2], dtype=np.float32)

        o_path = temp_dir + "/export.nvi.nc"
        args = export_nvi.ExportNviArg(
            o_path=o_path,
            time=times_in_ns,
            latitude=o_latitudes,
            longitude=o_longitudes,
            quality_flag=o_quality_flag,
            heading=o_headings,
            heading_rate=o_heading_rates,
            pitch=o_pitchs,
            roll=o_rolls,
            height_above_reference_ellipsoid=o_height_above_reference_ellipsoid,
            vertical_offset=None,
        )

        export_nvi.exports_with_ExportNviArg(args)

        with nvi_v2.xarray_open(o_path) as dataset:
            # Dimensions
            assert dataset.dims["time"] == 2

            # Coordinate variables
            first_date = dataset.coords["time"].values[0]
            assert first_date == np.datetime64("2012-06-07 08:36:35.328")

            assert_almost_equal(dataset.coords["latitude"].values[0], 43.01, decimal=2)
            assert_almost_equal(dataset.coords["longitude"].values[0], -1.01, decimal=2)

            # Variables

            assert_almost_equal(dataset.data_vars["heading"].values[0], 45.5, decimal=2)
            assert_almost_equal(
                dataset.data_vars["heading_rate"].values[0], 11.1, decimal=2
            )
            assert_almost_equal(
                dataset.data_vars["height_above_reference_ellipsoid"].values[0],
                10.1,
                decimal=2,
            )
            assert dataset.data_vars["quality_flag"].values[0] == 2
            assert_almost_equal(dataset.data_vars["pitch"].values[0], 0.1, decimal=2)
            assert_almost_equal(dataset.data_vars["roll"].values[0], 0.2, decimal=2)

            # vertical_offset not supplied. all values are NaN
            assert np.isnan(dataset.data_vars["vertical_offset"].values[0])
