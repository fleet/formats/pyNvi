import glob
import os.path
import tempfile
from osgeo import ogr
import pynvi.legacy.nvi_driver as nvi
from pynvi.legacy.exporter import ConvertNvi2Shp
from .file_installer import get_test_path


NVI_PATH = get_test_path().absolute() / "nvi" / "0136_20120607_083636_ShipName_ref.nvi"

def silent_delete(output_file_pattern:str):
    try:
        to_delete = glob.glob(output_file_pattern + ".*")
        for f in to_delete:
            os.remove(f)
    except Exception as e:
        print(e)

def test_nvi():
    with nvi.nc_open(NVI_PATH) as f:
        lat = f.get_latitudes()
        assert len(lat)>0

def test_export2shape():
    try:
        output_file_pattern = tempfile.mktemp()
        #remove old generated files

        output_file = output_file_pattern+".shp"

        assert os.path.exists(NVI_PATH)
        converter = ConvertNvi2Shp(input_files=[NVI_PATH],output_file=output_file)
        converter()
        assert os.path.exists(output_file)

        driver = ogr.GetDriverByName('ESRI Shapefile')
        data_source = driver.Open(output_file, 0)  # 0 means read-only. 1 means writeable
        layer = data_source.GetLayer()
        feature = layer.GetFeature(0)
        geom = feature.GetGeometryRef()
        assert geom.GetPointCount() == 842


    finally:
        # do some silent cleanup
        data_source = None
        silent_delete(output_file_pattern=output_file_pattern)
    return True
