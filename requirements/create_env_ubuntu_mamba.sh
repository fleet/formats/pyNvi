#!/bin/bash

#install mamba if not already done
conda install mamba -c conda-forge

mamba env remove --name pynvi

mamba env create -f requirements.yml --name pynvi
