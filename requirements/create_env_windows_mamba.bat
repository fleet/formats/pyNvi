REM install mamba if not already done
call conda install mamba -c conda-forge -y

echo mamba env remove --name pynvi_env -y
call mamba env remove --name pynvi_env -y

echo mamba env create -f requirements.yml --name pynvi_env
call mamba env create -f requirements.yml --name pynvi_env

